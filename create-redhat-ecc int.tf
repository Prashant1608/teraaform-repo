provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "RedHat" {
  ami           = "ami-04e81358dc84d661d"
  instance_type = "t2.micro"

  tags = {
    Name ="RedHat"
    }
}